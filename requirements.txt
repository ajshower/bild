attrs==20.3.0
bandit==1.7.0
certifi==2020.12.5
chardet==4.0.0
click==7.1.2
coverage==5.3.1
dparse==0.5.1
flake8==3.8.4
flake8-polyfill==1.0.2
gitdb==4.0.5
GitPython==3.1.11
hachoir==3.1.1
idna==2.10
iniconfig==1.1.1
isort==5.6.4
lazy-object-proxy~=1.4
mccabe==0.6.1
mypy==0.790
mypy-extensions==0.4.3
packaging==20.8
pbr==5.5.1
pep8-naming==0.11.1
pluggy==0.13.1
py==1.10.0
pycodestyle==2.6.0
pyflakes==2.2.0
pylint==2.6.0
pyparsing==2.4.7
pytest==6.2.1
pytest-cov==2.10.1
pytest-datafiles==2.0
PyYAML==5.3.1
requests==2.25.1
safety==1.10.0
six==1.15.0
smmap==3.0.4
stevedore==3.3.0
toml==0.10.2
typed-ast==1.4.1
typing-extensions==3.7.4.3
urllib3==1.26.2
wrapt==1.12.1
